if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

function no_connection(){
    $("#Params").hide();
    $("#ListeAnims").hide();
    $("#WebGL").hide();
    $("#navbar_bottom").hide();
};

no_connection();

// Variables globales
var SERVEUR = 'ws://' + getCookie('ip_adress') + ':8080/ws';
var g_cases;
var g_matrix;
var g_matrix_infos;
var is_mobile = window.matchMedia("(min-width: 400px)").matches;
var minWidthHeight = Math.min(window.innerWidth, window.innerHeight-100);


function setCookie(sName, sValue) {
    var today = new Date(), expires = new Date();
    expires.setTime(today.getTime() + (365*24*60*60*1000));
    document.cookie = sName + "=" + encodeURIComponent(sValue) + ";expires=" + expires.toGMTString();
};

function getCookie(sName) {
    var oRegex = new RegExp("(?:; )?" + sName + "=([^;]*);?");

    if (oRegex.test(document.cookie)) {
        return decodeURIComponent(RegExp["$1"]);
    } else {
        return null;
    }
};

function resetCookie(sName) {
    var today = new Date(), expires = new Date();
    expires.setTime(today.getTime() - 1000);
    document.cookie = sName + "=" + encodeURIComponent('nada') + ";expires=" + expires.toGMTString();
};

function ValidateIPaddress(ipaddress) {  
    if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {  
        return (true)
    }  
    return (false)  
};

$("#form_ipAdress").submit(function(e){
    var ddo_server_ip = $("#ipInput").val();
    if( ddo_server_ip == "demo" ){
        ddo_server_ip = '91.121.194.104';
    }
    if (ValidateIPaddress(ddo_server_ip)) {

        setCookie('ip_adress', ddo_server_ip);

        SERVEUR = 'ws://' + ddo_server_ip + ':8080/ws';
    }
});

$("#server_adress").empty().append(getCookie('ip_adress'));

// Début du code AutobahnJS
var connection = new autobahn.Connection({
    url: SERVEUR,
    realm: 'realm1'
});

connection.onopen = function (session) {
    $("#lost_connectivity").hide();
    $("#WebGL").show();
    $("#navbar_bottom").show();
    
//    session.publish('com.myapp.hello', ['Hello, world!']);

    // On fait un premier appel pour déterminer combien de cases il faut aux matrices
    // Et pour afficher les bonnes icones
    session.call('bzh.splann3.server.infos').then(
        function (res) {
            g_cases = res.server.cases;
            // On initialise la matrice a la bonne taille, et on met du noir dedans
            g_matrix = new Array(g_cases);
            for( var x = 0; x < g_cases; x++ ) {
                g_matrix[x] = new Array(g_cases);
                for( var y = 0; y < g_cases; y++) {
                    g_matrix[x][y] = new Array(0, 0, 0);
                }
            }

            init_webgl();
        }
    );

    // A chaque nouvelle matrice reçue, on rempli le tableau qui va bien
    function onmatrix(args) {
        for( var x = 0; x < g_cases; x++ ) {
            for( var y = 0; y < g_cases; y++) {
                for( var c = 0; c < 3; c++) {
                    g_matrix[x][y][c] = args[0][x][y][c];
                }
            }
        }
    }
    session.subscribe('bzh.splann3.anim.matrix', onmatrix);
    
    function onmatrixinfos(args) {
        var texte =  args[0].animation_name + "<br />\n";
        texte +=  args[0].frame + "/" +  args[0].frames_total + " | \n";
        texte += "delai : " +  args[0].delai + "<br />\n";
        $("#container_osd").empty().append(texte);
    }
    session.subscribe('bzh.splann3.anim.infos', onmatrixinfos);

    function onchange(args) {
        update_icons();
        
    }
    session.subscribe('bzh.splann3.change', onchange);
    
    function ondraw(args) {
// 		console.log(args[0]);
        if( args[0] == true){
            $("#draw_button").addClass('btn-primary');
        } else{
            $("#draw_button").removeClass('btn-primary');
        }
    }
    session.subscribe('bzh.splann3.draw', ondraw);

    function update_icons() {
        function background_color_button(filter_color){
            if( filter_color == 0 ){
                $("#icon_color2").removeClass('glyphicon-adjust');
                $("#icon_color2").removeClass('glyphicon-tint');
                $('#color_button').css('background', '#FFF');
            } else if( filter_color == 1 ){
                $("#icon_color2").removeClass('glyphicon-adjust');
                $("#icon_color2").addClass('glyphicon-tint');
                $('#color_button').css('background', 'linear-gradient(to right, #FF0 , #0FF)');
            } else {
                $("#icon_color2").addClass('glyphicon-adjust');
                $("#icon_color2").removeClass('glyphicon-tint');
                $('#color_button').css('background', 'linear-gradient(to right, #F00, #0F0, #0F0, #0F0, #0F0 ,#0F0 , #00F)');
            };
        }
        $("#icon_pause").hide();
        $("#icon_audio").hide();
        $("#icon_test").hide();
        $("#icon_color").hide();
        $("#icon_stick").hide();
        $("#icon_serial").hide();
        $("#pause_button").removeClass('active');
        $("#stick_button").removeClass('active');
        $("#serial_button").removeClass('active');
        $("#test_button").removeClass('active');
        $("#audio_button").removeClass('active');
        session.call('bzh.splann3.server.infos').then(
            function (res) {
                if (res.server.pause) {
                    $("#icon_pause").show();
                    $("#pause_button").addClass('active');
                }
                if (res.server.stick) {
                    $("#icon_stick").show();
                    $("#stick_button").addClass('active');
                }
                if (res.server.serial) {
                    $("#icon_serial").show();
                    $("#serial_button").addClass('active');
                }
                if (res.filters.test) {
                    $("#icon_test").show();
                    $("#test_button").addClass('active');
                }
                if (res.filters.audio) {
                    $("#icon_audio").show();
                    $("#audio_button").addClass('active');
                }
                if (res.filters.color == 1) {
                    $("#icon_color").removeClass('glyphicon-adjust');
                    $("#icon_color").addClass('glyphicon-tint');
                    $("#icon_color").show();
                    color_button
                } else if (res.filters.color == 2) {
                    $("#icon_color").addClass('glyphicon-adjust');
                    $("#icon_color").removeClass('glyphicon-tint');
                    $("#icon_color").show();
                }
                background_color_button(res.filters.color);
            }
        );
    }
    
    $("#zap_button").on('click', function(){
        session.call('bzh.splann3.anims.zap');
    });
    $("#draw_button").on('click', function(){
        session.call('bzh.splann3.anims.zap', ['anims.deplacements.Dessine']);
    });
    
    // Début de WebGL et de KineticJS du coup, aussi
    function init_webgl() {
        $("#Params").hide();
        $("#ListeAnims").hide();
        
        //KineticJS
        var color_draw = new THREE.Color();
        color_draw.setHSL(  Math.random(), 1.0, 0.5 );
        
        var guid = (function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return function () {
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                    s4() + '-' + s4() + s4() + s4();
            };
        })();

        var getNewCircle = function (stage, color) {
            var circle = new Kinetic.Circle({
                x: stage.getWidth() / 2,
                y: stage.getHeight() / 2,
                radius: stage.getWidth() / 20,
                fill: color,
                stroke: 'white',
                strokeWidth: 1
            });
            return circle;
        };


        var createTimeout = function(circle, circles, layer){
            return window.setTimeout(function(){
                circle.o.remove();
                layer.draw();
                var index = circles.indexOf(circle);
                if (index > -1) {
                    circles.splice(index, 1);
                }
            }, 5000);
        };


        var uuid = guid();
        
        var color = 'rgb('+ Math.floor(color_draw.r*255) +','+ Math.floor(color_draw.g*255) +','+ Math.floor(color_draw.b*255) +')';

        var stage = new Kinetic.Stage({
            container: "container_kinetic",
            width: 10,
            height: 10
        });

        var layer = new Kinetic.Layer();

        var rect = new Kinetic.Rect({
            height: 10,
            width: 10
        });

        layer.on('mousemove', function (e) {
            var tab_touch = [];
            tab_touch.push({
                    'x': e.evt.layerX/stage.width(),
                    'y': e.evt.layerY/stage.width(),
                    'hue': color_draw.getHSL().h
            });
            handleMove({
                x: e.evt.layerX,
                y: e.evt.layerY,
                id: 0,
                uuid: uuid,
                color: color
            });
            session.publish('bzh.splann3.touch', [tab_touch]);
        });

        layer.on('touchmove', function (e) {
            var tab_touch = [];
            for (var i = 0; i < e.evt.targetTouches.length; i++) {
                var touch = e.evt.targetTouches[i];
                tab_touch.push({
                        'x': touch.clientX/stage.width(),
                        'y': touch.clientY/stage.width(),
                        'hue': color_draw.getHSL().h
                });
                handleMove({
                    x: touch.clientX,
                    y: touch.clientY,
                    id: i,
                    uuid: uuid,
                    color: color
                });
            };
            session.publish('bzh.splann3.touch', [tab_touch]);
        });

        stage.add(layer);
        layer.add(rect);

        var circles = [];

        var handleMove = function (pos) {
    //         pos.x = pos.x / stage.width();
    //         pos.y = pos.y / stage.height();

            var circle = null;

            for (var i = 0; i < circles.length; i++) {
                if (pos.uuid == circles[i].uuid && pos.id == circles[i].id) {
                    circle = circles[i];
                }
            }

            if (circle == null) {
                circle = {uuid: pos.uuid,
                    id: pos.id,
                    o: getNewCircle(stage, color),
                    timeout: createTimeout(circle, circles, layer)
                };
                if(uuid != pos.uuid){
                    circle.o.setFill(pos.color);
                }
                circles.push(circle);
                layer.add(circle.o);
            }else{
                window.clearTimeout(circle.timeout);
                circle.timeout = createTimeout(circle, circles, layer);
            }

    //         pos.x = pos.x * stage.width();
    //         pos.y = pos.y * stage.height();

            circle.o.position(pos);

            layer.draw();
        };

        var resize = function () {
            stage.setHeight(minWidthHeight);
            stage.setWidth(minWidthHeight);
            rect.setHeight(minWidthHeight);
            rect.setWidth(minWidthHeight);

            layer.draw();
            $('#kinetic').css('width', minWidthHeight);
        };

        resize();
        //!KineticJS
        
        
        var renderer, scene, camera;

        var particleSystem, uniforms, geometry;

        var particles = g_cases*g_cases;
        

        init();
        animate();

        function init() {
            //camera = new THREE.PerspectiveCamera( 40, 1, 1, 10000 );
            var camWH = minWidthHeight / 9;
            camera = new THREE.OrthographicCamera( -camWH, camWH, camWH, -camWH, 1, 2*minWidthHeight );
            camera.position.z = 0.6*minWidthHeight;

            scene = new THREE.Scene();

            var attributes = {
                size:        { type: 'f', value: null },
                customColor: { type: 'c', value: null }
            };

            uniforms = {
                color:     { type: "c", value: new THREE.Color( 0xffffff ) },
                texture:   { type: "t", value: THREE.ImageUtils.loadTexture( "spark1.png" ) }
            };

            var shaderMaterial = new THREE.ShaderMaterial( {
                uniforms:       uniforms,
                attributes:     attributes,
                vertexShader:   document.getElementById( 'vertexshader' ).textContent,
                fragmentShader: document.getElementById( 'fragmentshader' ).textContent,

                blending:       THREE.AdditiveBlending,
                depthTest:      false,
                transparent:    true
            });

            geometry = new THREE.BufferGeometry();

            var positions = new Float32Array( particles * 3 );
            var values_color = new Float32Array( particles * 3 );
            var values_size = new Float32Array( particles );
            var color = new THREE.Color();
            color.setHSL( 0.0, 1.0, 0.0 );
            var v = 0;
            // La taille doit être inversement proportionnelle au nombre de cases
            // et proportionelle à la taille de la fenêtre
            var calc_size;
            if (is_mobile == false) {
                calc_size = 5000;//10.75*minWidthHeight/g_cases;
            } else {
                calc_size = 0.25*minWidthHeight/g_cases;
            }
            for( var x = 0; x < g_cases; x++ ) {
                for( var y = 0; y < g_cases; y++ ) {
                    values_size[ v ] = calc_size;

                    positions[ v * 3 + 0 ] = ((g_cases-1)/2-x)*(0.2/g_cases)*minWidthHeight; //X
                    positions[ v * 3 + 1 ] = ((g_cases-1)/2-y)*(0.2/g_cases)*minWidthHeight; //Y
                    positions[ v * 3 + 2 ] = 0; //Z

                    values_color[ v * 3 + 0 ] = color.r;
                    values_color[ v * 3 + 1 ] = color.g;
                    values_color[ v * 3 + 2 ] = color.b;
                    v++;
                }
            }

            geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) );
            geometry.addAttribute( 'customColor', new THREE.BufferAttribute( values_color, 3 ) );
            geometry.addAttribute( 'size', new THREE.BufferAttribute( values_size, 1 ) );

            particleSystem = new THREE.PointCloud( geometry, shaderMaterial );

            scene.add( particleSystem );

            renderer = new THREE.WebGLRenderer();
            renderer.setSize( minWidthHeight, minWidthHeight );

            var container_webgl = document.getElementById( 'container_webgl' );
            container_webgl.appendChild( renderer.domElement );

            window.addEventListener( 'resize', onWindowResize, false );
            
            update_icons();
        }
        


        function onWindowResize() {
            var minWidthHeight = Math.min(window.innerWidth, window.innerHeight-100);
            camera.aspect = 1;
            camera.updateProjectionMatrix();

            renderer.setSize( minWidthHeight, minWidthHeight );
            resize(); // KineticJS
        }

        function animate() {
            var color = geometry.attributes.customColor.array;
            var i = 0;
            for( var x = 0; x < g_cases; x++){
                for( var y = 0; y < g_cases; y++){
                    color[ i * 3 + 0 ] = g_matrix[g_cases-1-x][y][0]/255;
                    color[ i * 3 + 1 ] = g_matrix[g_cases-1-x][y][1]/255;
                    color[ i * 3 + 2 ] = g_matrix[g_cases-1-x][y][2]/255;
                    i++;
                }
            }
            requestAnimationFrame( animate );

            render();
        }
        
        function render() {
            geometry.attributes.customColor.needsUpdate = true;
            renderer.render( scene, camera );
        }
    };
    // Fin de init_webgl

    function aff_webgl() {
        $("#list_button").removeClass("btn-primary");
        $("#pref_button").removeClass("btn-primary");
        $("#Params").hide();
        $("#ListeAnims").hide();
        $("#WebGL").show();
    };

    function aff_params() {
        $("#list_button").removeClass("btn-primary");
        $("#pref_button").addClass("btn-primary");
        $("#Params").show();
        $("#ListeAnims").hide();
        $("#WebGL").hide();
        init_params();
    };

    function init_params(){
        function HSVtoRGB(h, s, v) {
            var r, g, b, i, f, p, q, t;
            if (h && s === undefined && v === undefined) {
                s = h.s, v = h.v, h = h.h;
            }
            i = Math.floor(h * 6);
            f = h * 6 - i;
            p = v * (1 - s);
            q = v * (1 - f * s);
            t = v * (1 - (1 - f) * s);
            switch (i % 6) {
                case 0: r = v, g = t, b = p; break;
                case 1: r = q, g = v, b = p; break;
                case 2: r = p, g = v, b = t; break;
                case 3: r = p, g = q, b = v; break;
                case 4: r = t, g = p, b = v; break;
                case 5: r = v, g = p, b = q; break;
            }
            return {
                r: Math.floor(r * 255),
                g: Math.floor(g * 255),
                b: Math.floor(b * 255)
            };
        };
        
        var DelaiChange = function() {
            var delai = slider_delai.getValue();
            session.call('bzh.splann3.anim.delai', [delai])
        };
        var TeinteChange = function() {
            var teinte = slider_teinte.getValue()/360;
            color = HSVtoRGB(teinte,1,1)
            $('#teinte_bg').css('background', 'linear-gradient(to right, #FFF ,rgb(' + color['r'] + ',' + color['g'] + ',' + color['b'] + '))');
            session.call('bzh.splann3.color.hue', [teinte])
        };
        var DeltaChange = function() {
            var delta = slider_delta.getValue()/100;
            session.call('bzh.splann3.color.delta', [delta])
        };
        var VariaChange = function() {
            var varia = slider_varia.getValue();
            session.call('bzh.splann3.color.variation', [varia])
        };
        session.call('bzh.splann3.server.infos').then(
            function (res) {
                $('#slider_delai').slider('setValue', res.infos.delai);
                $('#slider_teinte').slider('setValue', Math.round(res.infos.hue_global*360));
                TeinteChange();
                $('#slider_delta').slider('setValue', Math.round(res.infos.delta_global*100));
                $('#slider_varia').slider('setValue', res.infos.variation_global);
            }
        );
        
        var slider_delai = $('#slider_delai').slider().on('slideStop', DelaiChange).data('slider');
        var slider_teinte = $('#slider_teinte').slider().on('slideStop', TeinteChange).data('slider');
        var slider_delta = $('#slider_delta').slider().on('slideStop', DeltaChange).data('slider');
        var slider_varia = $('#slider_varia').slider().on('slideStop', VariaChange).data('slider');

        $("#pause_button").on('click', function(){
            session.call('bzh.splann3.anim.pause').then(
                function (res) {
                    if( res == true ){
                        session.call('bzh.splann3.anim.pause', ["false"])
                    } else {
                        session.call('bzh.splann3.anim.pause', ["true"])
                    }
                }
            );
        });
        $("#stick_button").on('click', function(){
            session.call('bzh.splann3.anim.stick').then(
                function (res) {
                    if( res == true ){
                        session.call('bzh.splann3.anim.stick', ["false"])
                    } else {
                        session.call('bzh.splann3.anim.stick', ["true"])
                    }
                }
            );
        });

        $("#color_button").on('click', function(e){

            session.call('bzh.splann3.anims.filters.color').then(
                function (res) {
                    session.call('bzh.splann3.anims.filters.color', [(res + 1)%3])
                }
            );
        });
        $("#audio_button").on('click', function(e){
            session.call('bzh.splann3.anims.filters.audio').then(
                function (res) {
                    if( res == true ){
                        session.call('bzh.splann3.anims.filters.audio', ["false"])
                    } else {
                        session.call('bzh.splann3.anims.filters.audio', ["true"])
                    }
                }
            );
        });
        $("#test_button").on('click', function(e){
            session.call('bzh.splann3.anims.filters.test').then(
                function (res) {
                    if( res == true ){
                        session.call('bzh.splann3.anims.filters.test', ["false"])
                    } else {
                        session.call('bzh.splann3.anims.filters.test', ["true"])
                    }
                }
            );
        });
        $("#serial_button").on('click', function(e){
            session.call('bzh.splann3.server.serial').then(
                function (res) {
                    if( res == true ){
                        session.call('bzh.splann3.server.serial', ["false"])
                    } else {
                        session.call('bzh.splann3.server.serial', ["true"])
                    }
                }
            );
        });
        $("#deconection").on('click', function(e){
            resetCookie('ip_adress');
            SERVEUR = '';
            $.ajax({
                url: "",
                context: document.body,
                success: function(s,x){
                    $(this).html(s);
                }
            });
        });

    };
    // fin de init_params

    
    select_anim = function (anim){
        session.call('bzh.splann3.anims.zap', [anim]);//.then(
        init_anims();
        return false;
    };

    add_anim = function (event, anim){
        event.stopPropagation();
        session.call('bzh.splann3.anims.add', [anim]);
        init_anims();
        return false;
    };

    function aff_anims() {
        $("#list_button").addClass("btn-primary");
        $("#pref_button").removeClass("btn-primary");
        $("#Params").hide();
        $("#ListeAnims").show();
        $("#WebGL").hide();
        pause_WebGL = true;
        init_anims();
    };

    function init_anims(){
        session.call('bzh.splann3.anims').then(
            function (res) {
                var html = '<div class="list-group">\n';
                res.forEach(function(item) {
                    html = html + '<a class="list-group-item" onclick="javascript:select_anim(\'' + item + '\'); return false;">' + item.split('.')[item.split('.').length -1] + '<button class=" btn btn-primary pull-right btn-xs" onclick="javascript:add_anim(event, \'' + item + '\'); return false;">+</button></a>';
                });
                html = html + '</div>\n'
                $("#anims").empty().append(html);
            }
        );
        
        session.call('bzh.splann3.anims.playlist').then(
            function (res) {
                var index = 0;
                var html = '<div class="list-group">\n';
                res.forEach(function(item) {
                    index++;
                    html = html + '<div class="list-group-item">' + index + ": " + item.split('.')[item.split('.').length -1] + '</div>\n';
                });
                html = html + '</div>\n'
                $("#playlist").empty().append(html);
            }
        );

    };
    // fin de anims

    $("#list_button").on('click', function(e){
        if ($("#list_button").hasClass("btn-primary")) {
            aff_webgl();
        } else {
            aff_anims();
        }
    });

    $("#pref_button").on('click', function(e){
        if ($("#pref_button").hasClass("btn-primary")) {
            aff_webgl();
        } else {
            aff_params();
        }
    });

};

connection.onclose = function(){
    no_connection();
};

connection.open();
// Fin du code AutobahnJS
